package spring.spring.annotation.config.demo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("hello")
public class HelloService implements IService {

	@Override
	public String retrieveMessage() {
		return "Hello From ISR";
	}

}
