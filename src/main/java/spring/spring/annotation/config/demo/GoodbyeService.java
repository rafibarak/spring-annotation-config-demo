package spring.spring.annotation.config.demo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("goodbye")
public class GoodbyeService implements IService {

	@Override
	public String retrieveMessage() {
		return "Goodbay.";
	}

}
