package spring.spring.annotation.config.demo;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

	//@Autowired
	private IService service;
	
	public Application(){
		super();
		System.err.println("Application");
		
	}
	
	//@Autowired
	public Application(IService service) {
		super();
		System.err.println("Application(service)");
		this.service=service;
	}
	
	@Autowired
	public void setService(IService service) {
		this.service = service;
	}

	@PostConstruct
	private void run() {
		System.err.println(service.retrieveMessage());

	}
	
	@Bean
	public IService helloService() {
		return new HelloService();
	}
	
	@Bean
	@Qualifier("goodbye")
	public IService goodByeService() {
		return new GoodbyeService();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
